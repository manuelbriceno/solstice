//
//  Contact.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import Foundation

class Contact {
    
    let employeeId: Int
    let name: String
    let company: String
    let detailsURL: String
    let smallImageURL: String
    let birthdate: String
    let phone: [Phone]
    var contactDetail: ContactDetail? = nil
    let sectionCount: Int = 4
    let dateFormatter = NSDateFormatter()
    
    init(data: JSON) {
        self.employeeId = data["employeeId"].int!
        self.name = data["name"].string!
        self.company = data["company"].string!
        self.detailsURL = data["detailsURL"].string!
        self.smallImageURL = data["smallImageURL"].string!
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        self.birthdate = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: Double(data["birthdate"].string!)!))
        var phoneList: [Phone] = []
        if let _mobile = data["phone"]["mobile"].string {
            if _mobile != "" {
                phoneList.append(Phone(type: "Mobile", number: _mobile))
            }
        }
        if let _work = data["phone"]["work"].string {
            if _work != "" {
                phoneList.append(Phone(type: "Work", number: _work))
            }
        }
        if let _home = data["phone"]["home"].string {
            if _home != "" {
                phoneList.append(Phone(type: "Home", number: _home))
            }
        }
        self.phone = phoneList
    }
    
    func getPrimaryPhone() -> String{
        return self.phone[0].number
    }
    
    func setContactDetail(contactDetail: ContactDetail) {
        self.contactDetail = contactDetail
    }
    
}
