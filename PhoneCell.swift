//
//  PhoneCell.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import UIKit

class PhoneCell: UITableViewCell {
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var call: UIButton!
    
}