//
//  DetailViewController.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import AddressBook

class DetailViewController: UITableViewController {
    
    var contact: Contact? = nil
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var work: UIButton!
    @IBOutlet weak var profile_picture: UIImageView!
    @IBOutlet weak var favorite: UIImageView!
    var mapAddress: Address? = nil;
    var webSiteUrl: String? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        work.setTitle(self.contact?.company, forState: UIControlState.Normal)
        if let imageUrl = self.contact?.smallImageURL {
            profile_picture.downloadFrom(link: imageUrl, contentMode: UIViewContentMode.ScaleAspectFit, completion: nil)
        }
        name.text = contact?.name
        if let detailsURL = self.contact?.detailsURL {
            Alamofire.request(.GET, detailsURL)
                .responseJSON { response in
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        self.contact?.setContactDetail(ContactDetail(data: json ))
                        if let imageUrl = self.contact?.contactDetail?.largeImageURL {
                            self.profile_picture.downloadFrom(link: imageUrl, contentMode: UIViewContentMode.ScaleAspectFill, completion: nil)
                        }
                        self.favorite.hidden = !(self.contact?.contactDetail?.favorite)!
                        self.mapAddress = self.contact?.contactDetail?.address
                        self.webSiteUrl = self.contact?.contactDetail?.website
                        self.work.addTarget(self, action: "openWebsite", forControlEvents: UIControlEvents.TouchUpInside)
                        self.tableView.reloadData()
                    }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("PhoneCell", forIndexPath: indexPath) as! PhoneCell
            cell.number.text = self.contact?.phone[indexPath.row].number
            cell.type.text = self.contact?.phone[indexPath.row].type
            cell.call.tag = indexPath.row
            cell.call.addTarget(self, action: "makeCall:", forControlEvents: UIControlEvents.TouchUpInside)
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddressCell", forIndexPath: indexPath) as! AddressCell
            cell.line_1.text = self.contact?.contactDetail?.address.street
            cell.line_2.text = self.contact?.contactDetail?.address.cityState
            cell.map.addTarget(self, action: "openMap", forControlEvents: UIControlEvents.TouchUpInside)
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("LabelCell", forIndexPath: indexPath) as! LabelCell
            if indexPath.section == 2 {
                cell.label.text = self.contact?.birthdate
            }else {
                cell.label.text = self.contact?.contactDetail?.email
            }
            return cell
        }
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (self.contact?.sectionCount)!
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ((self.contact?.contactDetail) != nil) {
            if section == 0 {
                return (self.contact?.phone.count)!
            }else {
                return 1
            }
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Phone"
        }else if section == 1 {
            return "Address"
        }else if section == 2 {
            return "Birthday"
        }else {
            return "Email"
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 70
        } else {
            return 40
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func openWebsite() {
        UIApplication.sharedApplication().openURL(NSURL(string: self.webSiteUrl!)!)
    }
    
    func openMap() {
        let coords = CLLocationCoordinate2DMake((mapAddress?.latitude)!, (mapAddress?.longitude)!)
        let addressDict: [String: String] = [
            kABPersonAddressStreetKey as String: (mapAddress?.street)!,
            kABPersonAddressCityKey as String: (mapAddress?.city)!,
            kABPersonAddressStateKey as String: (mapAddress?.state)!,
        ]
        let place = MKPlacemark(coordinate: coords, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: place)
        mapItem.openInMapsWithLaunchOptions(nil)
    }
    
    @IBAction func makeCall(sender:UIButton) {
        let phone : String = (self.contact?.phone[sender.tag].number)!
        if let url = NSURL(string: "tel://" + phone) {
            UIApplication.sharedApplication().openURL(url)
        }
    }

}