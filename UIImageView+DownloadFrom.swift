//
//  UIImageView+DownloadFrom.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func downloadFrom(link link:String, contentMode mode: UIViewContentMode, completion: ((result: UIImage?)->())!) {
        contentMode = mode
        if let url = NSURL(string: link) {
            NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: { (data, _, error) -> Void in
                guard let data = data where error == nil else { return }
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    let image_: UIImage? = UIImage(data: data)
                    self.image = image_
                    if completion != nil {
                        completion(result: image_)
                    }
                }
            }).resume()
        }
    }
}
