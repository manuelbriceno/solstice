//
//  ContactDetail.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import Foundation

class ContactDetail {
    
    let favorite: Bool
    let largeImageURL: String
    let email: String
    let website: String
    let address: Address
    
    init(data: JSON) {
        if let favorite = data["favorite"].bool {
            self.favorite = favorite
        }else {
           self.favorite = data["favorite"].int != 0
        }
        self.largeImageURL = data["largeImageURL"].string!
        self.email = data["email"].string!
        self.website = data["website"].string!
        self.address = Address(data:data["address"])
    }
    
}
