//
//  Address.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import Foundation

class Address {
 
    let street: String
    let city: String
    let state: String
    let country: String
    let zip: String
    let latitude: Double
    let longitude: Double
    let cityState: String
    
    init(data: JSON) {
        self.street = data["street"].string!
        self.city = data["city"].string!
        self.state = data["state"].string!
        self.country = data["country"].string!
        self.zip = data["zip"].string!
        self.latitude = data["latitude"].double!
        self.longitude = data["longitude"].double!
        self.cityState = data["city"].string! + ", " + data["state"].string!
    }
    
}
