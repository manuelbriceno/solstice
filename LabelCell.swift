//
//  LabelCell.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015 Opratel. All rights reserved.
//

import UIKit

class LabelCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    
}
