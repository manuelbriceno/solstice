//
//  ViewController.swift
//  Solstice
//
//  Created by Manuel Briceño on 29/11/15.
//  Copyright © 2015. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UITableViewController {
    
    var contactsList: [Contact] = []
    var imagesCache: [Int : UIImage] = [Int: UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request(.GET, URL_MAIN)
            .responseJSON { response in
                if let value = response.result.value {
                    let json = JSON(value)
                    print("JSON: \(json)")
                    for (index: _, subJson: contact) in json {
                        self.contactsList.append(Contact(data: contact ))
                    }
                    self.tableView.reloadData()
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContactListCell", forIndexPath: indexPath) as! ContactListCell
        
        let contact = contactsList[indexPath.row] as Contact
        cell.name.text = contact.name
        cell.number.text = contact.getPrimaryPhone()
        if let image: UIImage = imagesCache[indexPath.row] {
            cell.profile_picture.image = image
            
        } else {
            cell.profile_picture.downloadFrom(link: contact.smallImageURL, contentMode: UIViewContentMode.ScaleAspectFill,
                completion: { (result: UIImage?) in
                    self.imagesCache[indexPath.row] = result
            })
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        performSegueWithIdentifier("SelectContact", sender: contactsList[indexPath.row])
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SelectContact" {
            if let detailViewController = segue.destinationViewController as? DetailViewController {
                if let contact = sender as? Contact {
                    detailViewController.contact = contact
                }
            }
        }
    }

}

