//
//  AddressCell.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {
    @IBOutlet weak var line_1: UILabel!
    @IBOutlet weak var line_2: UILabel!
    @IBOutlet weak var map: UIButton!
    
}
