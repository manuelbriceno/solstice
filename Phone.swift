//
//  Phone.swift
//  Solstice
//
//  Created by Manuel Briceño on 3/12/15.
//  Copyright © 2015. All rights reserved.
//

import Foundation

class Phone {
    let type: String
    let number: String
    
    init(type: String, number: String) {
        self.type = type
        self.number = number
    }
}